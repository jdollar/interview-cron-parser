# Deliveroo Cron Parser

## Requirements:
* Node ^16
* asdf and nodejs plugin (optional)

## How to build and Run

This repository is setup with a `.tool-versions` file for easy setup
using [asdf](http://asdf-vm.com/guide/getting-started.html#_1-install-dependencies)
and the [nodejs](https://github.com/asdf-vm/asdf-nodejs) plugin for it.

Refer to the getting-started page for asdf for installation instructions for
your specific development environment if you want to go that route. Otherwise
as long as you have Node 16 installed things should work as expected.

```shell
# Install node version after asdf and nodejs plugin
# Ensure you are in the same directory as the .tool-versions file
asdf install
```

After having installed nodejs you should be set to build the script
and start parsing crone expressions. The flow for running the cron Parser
is as follows

```shell
# Install node dependencies
npm install

# Run the command with the input you want.
# This will trigger npm build as well.
npm start -- "0 * 2 * * /bin/sh test.sh"
```

The babel'ed script file can be found in the `dist` directory.

## Overview

All of the code for the script is located inside of the src/index.js file.
The gist of the script is that we separate all of the cron expression values (minutes, hours, etc)
based on the spaces in the statement and then for each individual section we read through the expression
and save data to a node. The node is just a data structure where the left most value is the current
expected result set based on the expression evaluated so far. When a operation is detected (comma, slash, hyphen)
we save that character to a 'operation' key on the node. Afterwards depending on the operation we either
grab the next integer value (/ or -) or take the remainder of a expression and treat it as a new expression (, operation).
Either case saves the result set as the 'right' value on a node and for each of the three situations we
complete an action on both the 'left' and 'right' node properties. Saving the result in a new
node's left value for continued processing.

The layout of the code is to start from the bottom and go up. Functions lower on the index.js file will reference
functions higher in the script. Asserts for validation are used to throw errors when invalid states occur.

## Tests

The code in the index.js script file is covered through tests found on the index.test.js file.
To run the scripts you can use jest or run:

```shell
npm test
```

This should execute all the tests for the cron parser and output the coverage for review.
