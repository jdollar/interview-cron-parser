import assert from 'assert'
import {
  applySpec,
  curry,
  cond,
  evolve,
  identity,
  path,
  pathEq,
  pipe,
  range,
  sort,
  split,
  tap,
  uniq
} from 'ramda'

const ALL_MINUTES = range(0, 60)
const ALL_HOURS = range(0, 60)
const ALL_MONTHS = range(1, 13)
const ALL_DAY_OF_MONTH = range(1, 32)
const ALL_DAY_OF_WEEK = range(0, 7)

const VALID_OPERATIONS = new Set(['/', '-', ','])

const operationEq = pathEq(['operation'])
const sortAsc = sort((a,b) => a - b)
const isNumber = (val) => !Number.isNaN(Number.parseInt(val, 10))

const dashOperation = (_opts) => (node) => {
  const value = []

  const { token: rightToken, expression } = getToken(node.expression)
  assert(isNumber(rightToken), 'Token provided for dash operation is not a valid number')
  node.expression = expression

  const start = node.left[0]
  const end = rightToken

  assert(start <= end, 'Minimum of dash should be larger than max in range')
  for (let i = start; i <= end; i++) {
    value.push(i)
  }

  return {
    left: value,
    expression
  }
}

const slashOperation = (_opts) => (node) => {
  const { token: rightToken, expression: rightTokenExpression } = getToken(node.expression)
  assert(isNumber(rightToken), 'Token provided for slash operation is not a valid number')

  const toCheck = node.left[0] % rightToken
  const value = node.left.filter((num) => (num % rightToken) === toCheck)

  return {
    left: value,
    expression: rightTokenExpression
  }
}

const commaOperation = (opts) => (node) => {
  const { value, expression } = processNode(opts, node.expression)

  return {
    left: [...node.left, ...value],
    expression
  }
}

const processOperation = (opts) => pipe(
  cond([
    [operationEq('-'), dashOperation(opts)],
    [operationEq('/'), slashOperation(opts)],
    [operationEq(','), commaOperation(opts)]
  ]),
  evolve({
    left: pipe(
      uniq,
      sortAsc
    )
  })
)

const getToken = (input) => {
  const expression = [...input]
  let token = ''

  while(expression.length > 0) {
    const peek = expression[0]

    const haveIntNextIsOperation = token.length > 0 && VALID_OPERATIONS.has(peek)
    const isTokenOperation = VALID_OPERATIONS.has(token)
    const haveFinishedToken = haveIntNextIsOperation || isTokenOperation
    if (haveFinishedToken) {
      break
    }

    token = token.concat(expression.shift())
  }

  return {
    token,
    expression
  }
}

const processNode = curry((opts, input) => {
  const result = []
  let node = {
    left: [],
    expression: [...input]
  }

  const processExpressionOperation = processOperation(opts)

  while(node.expression.length > 0) {
    if (VALID_OPERATIONS.has(node.expression[0])) {
      assert(isNumber(node.left), `No number to operate against for ${node.expression[0]}`)
      node.operation = node.expression.shift()
      node = processExpressionOperation(node)
      continue
    }

    const { token: foundToken, expression: tokenExpression } = getToken(node.expression)
    node.expression = tokenExpression

    node.left = foundToken === '*' ? opts.all : [Number(foundToken)]
  }

  result.push(...node.left)

  return {
    value: result,
    expression: node.expression
  }
})

const parseItem = (config) => pipe(
  path([config.index]),
  split(''),
  processNode(config),
  path(['value']),
  config.validate
)

const validateRange = ({ display, min, max }) => tap((possible) => {
  const insideRange = possible.every((val) => val >= min && val <= max)
  assert(insideRange, `${display} provided that are outside allowed range`)
})

const parseConfigs = {
  minutes: {
    index: 0,
    all: ALL_MINUTES,
    validate: pipe(
      validateRange({ display: 'minutes', min: 0, max: 59 })
    )
  },
  hours: {
    index: 1,
    all: ALL_HOURS,
    validate: pipe(
      validateRange({ display: 'hours', min: 0, max: 59 })
    )
  },
  dayOfMonth: {
    index: 2,
    all: ALL_DAY_OF_MONTH,
    validate: pipe(
      validateRange({ display: 'dayOfMonth', min: 1, max: 31 })
    )
  },
  months: {
    index: 3,
    all: ALL_MONTHS,
    validate: pipe(
      validateRange({ display: 'months', min: 1, max: 12 })
    )
  },
  dayOfWeek: {
    index: 4,
    all: ALL_DAY_OF_WEEK,
    validate: pipe(
      validateRange({ display: 'dayOfWeek', min: 0, max: 6 })
    )
  }
}

const parseMinute = parseItem(parseConfigs.minutes)
const parseHour = parseItem(parseConfigs.hours)
const parseDayOfMonth = parseItem(parseConfigs.dayOfMonth)
const parseMonth = parseItem(parseConfigs.months)
const parseDayOfWeek = parseItem(parseConfigs.dayOfWeek)

const parseCommand = (inputs) => {
  return inputs.slice(5).join(' ')
}

const parseCron = applySpec({
  minute: parseMinute,
  hour: parseHour,
  dayOfMonth: parseDayOfMonth,
  month: parseMonth,
  dayOfWeek: parseDayOfWeek,
  command: parseCommand
})

export const main = (inputs) => {
  assert(Array.isArray(inputs) && inputs.length >= 1, 'Must provide complete cron expression and script path')
  const scriptInput = inputs[0].split(' ')
  assert(Array.isArray(scriptInput) && scriptInput.length >= 6, 'Must provide complete cron expression and script path')

  return parseCron(scriptInput)
}

/* istanbul ignore next untestable */
if (require.main === module) {
  const result = main(process.argv.slice(2))
  console.log('result', result)
}
