import { range } from 'ramda'
import { main } from '.'

const createMockInput = (opts) => {
  const {
    minutes = '*',
    hours = '*',
    dayOfMonth = '*',
    months = '*',
    dayOfWeek = '*',
    command = '/bin/sh script.sh'
  } = opts

  return [`${minutes} ${hours} ${dayOfMonth} ${months} ${dayOfWeek} ${command}`]
}

const ALL_MINUTES = range(0, 60)
const ALL_HOURS = range(0, 60)
const ALL_MONTHS = range(1, 13)
const ALL_DAY_OF_MONTH = range(1, 32)
const ALL_DAY_OF_WEEK = range(0, 7)

describe('cron-parser', () => {
  it.each([
    [[], 'Must provide complete cron expression and script path'],
    [null, 'Must provide complete cron expression and script path'],
    [createMockInput({ minutes: '2-a' }), 'Token provided for dash operation is not a valid number'],
    [createMockInput({ minutes: '2-1' }), 'Minimum of dash should be larger than max in range'],
    [createMockInput({ minutes: '*/a' }), 'Token provided for slash operation is not a valid number'],
    [createMockInput({ minutes: '/a' }), 'No number to operate against for /'],
    [createMockInput({ minutes: ',a' }), 'No number to operate against for ,'],
    [createMockInput({ minutes: '-a' }), 'No number to operate against for -'],
    // range
    [createMockInput({ minutes: '78' }), 'minutes provided that are outside allowed range'],
    [createMockInput({ minutes: '78' }), 'minutes provided that are outside allowed range'],
    [createMockInput({ hours: '78' }), 'hours provided that are outside allowed range'],
    [createMockInput({ months: '0' }), 'months provided that are outside allowed range'],
    [createMockInput({ months: '14' }), 'months provided that are outside allowed range'],
    [createMockInput({ dayOfMonth: '0' }), 'dayOfMonth provided that are outside allowed range'],
    [createMockInput({ dayOfMonth: '32' }), 'dayOfMonth provided that are outside allowed range'],
    [createMockInput({ dayOfWeek: '7' }), 'dayOfWeek provided that are outside allowed range'],
  ])('should error when inputs are invalid - %j', (invalidInputs, expectedError) => {
    expect(() => main(invalidInputs)).toThrowError(expectedError)
  })

  it.each([
    // Minutes
    [
      { minutes: '0' },
      { minute: [0] }
    ],
    [
      { minutes: '*' },
      { minute: ALL_MINUTES }
    ],
    [
      { minutes: '3-10' },
      { minute: [3, 4, 5, 6, 7, 8, 9, 10] }
    ],
    [
      { minutes: '*/10' },
      { minute: [0, 10, 20, 30, 40, 50] }
    ],
    [
      { minutes: '0-20/10' },
      { minute: [0, 10, 20] }
    ],
    [
      { minutes: '20,25,55' },
      { minute: [20, 25, 55] }
    ],
    [
      { minutes: '20-22,53-55' },
      { minute: [20, 21, 22,53, 54, 55] }
    ],
    // Hours
    [
      { hours: '0' },
      { hour: [0] }
    ],
    [
      { hours: '*' },
      { hour: ALL_HOURS }
    ],
    [
      { hours: '3-10' },
      { hour: [3, 4, 5, 6, 7, 8, 9, 10] }
    ],
    [
      { hours: '*/10' },
      { hour: [0, 10, 20, 30, 40, 50] }
    ],
    [
      { hours: '0-20/10' },
      { hour: [0, 10, 20] }
    ],
    [
      { hours: '20,25,55' },
      { hour: [20, 25, 55] }
    ],
    [
      { hours: '20-22,53-55' },
      { hour: [20, 21, 22,53, 54, 55] }
    ],
    // dayofMonth
    [
      { dayOfMonth: '1' },
      { dayOfMonth: [1] }
    ],
    [
      { dayOfMonth: '*' },
      { dayOfMonth: ALL_DAY_OF_MONTH }
    ],
    [
      { dayOfMonth: '2-5' },
      { dayOfMonth: [2, 3, 4, 5] }
    ],
    [
      { dayOfMonth: '*/5' },
      { dayOfMonth: [1, 6, 11, 16, 21, 26, 31]}
    ],
    [
      { dayOfMonth: '5-12/3' },
      { dayOfMonth: [5, 8, 11]}
    ],
    [
      { dayOfMonth: '2,3' },
      { dayOfMonth: [2, 3]}
    ],
    [
      { dayOfMonth: '2-5,7-9' },
      { dayOfMonth: [2, 3, 4, 5, 7, 8, 9] }
    ],
    // Months
    [
      { months: '1' },
      { month: [1] }
    ],
    [
      { months: '*' },
      { month: ALL_MONTHS }
    ],
    [
      { months: '2-5' },
      { month: [2, 3, 4, 5] }
    ],
    [
      { months: '*/2' },
      { month: [1, 3, 5, 7, 9, 11]}
    ],
    [
      { months: '5-12/3' },
      { month: [5, 8, 11]}
    ],
    [
      { months: '2,3' },
      { month: [2, 3]}
    ],
    [
      { months: '2-5,7-9' },
      { month: [2, 3, 4, 5, 7, 8, 9] }
    ],
    // dayOfWeek
    [
      { dayOfWeek: '0' },
      { dayOfWeek: [0] }
    ],
    [
      { dayOfWeek: '*' },
      { dayOfWeek: ALL_DAY_OF_WEEK }
    ],
    [
      { dayOfWeek: '2-5' },
      { dayOfWeek: [2, 3, 4, 5] }
    ],
    [
      { dayOfWeek: '*/5' },
      { dayOfWeek: [0, 5]}
    ],
    [
      { dayOfWeek: '3-6/3' },
      { dayOfWeek: [3, 6]}
    ],
    [
      { dayOfWeek: '2,3' },
      { dayOfWeek: [2, 3]}
    ],
    [
      { dayOfWeek: '1-3,5-6' },
      { dayOfWeek: [1, 2, 3, 5, 6] }
    ],
    [
      { command: '/bin/sh test.sh' },
      { command: '/bin/sh test.sh' }
    ]
  ])('should print out valid data - %j', (data, expected) => {
    const input = createMockInput(data)
    expect(main(input)).toEqual(expect.objectContaining(expected))
  })
})
